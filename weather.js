'use latest';

import express from 'express';
import {fromExpress} from 'webtask-tools';
import bodyParser from 'body-parser';
import fetch from 'node-fetch'
import handlebars from 'handlebars';
import moment from 'moment';

handlebars.registerHelper('ifCond', function(v1, v2, options) {
	v1 += '';
	v2 += '';
	if (v1 === v2) {
		return options.fn(this);
	}
	return options.inverse(this);
});

handlebars.registerHelper('round', function(value, options) {
	return Math.round(value);
});

const appPrefix = '/webtask-weather';
const app = express();
const apiUrl = 'https://api.openweathermap.org/data/2.5/';
const weatherQuery = 'weather?';
const forecastQuery = 'forecast?'
const zipQuery = 'zip=';
const cityIdQuery = 'id=';
const apiQuery = 'APPID=';
const metricQuery = 'units=imperial';
const nameQuery = 'q=';
const countryPrefix = ',US';
const storageError ='Webtask Storage error';

const getSecretFromReq = (request) => {
	return request.webtaskContext.secrets.API_KEY
};

const getByZip = (secret, zip) => {
	return fetchData(secret, zipQuery + zip + countryPrefix, 'forecast');
};

const getByCity = (secret, id) => {
	return fetchData(secret, cityIdQuery + id, 'forecast');
};

const getByName = (secret, name) => {
	return fetchData(secret, nameQuery + name + countryPrefix, 'forecast');
}

const fetchData = (secret, query, type) => {
	let url = apiUrl;

	switch (type) {
		case 'weather':
			url += weatherQuery;
			break;
		case 'forecast':
			url += forecastQuery;
			break;
		default:
			break;
	}

	url += [apiQuery + secret, metricQuery, query].join('&');

	return fetch(url).then(response => response.json());
};

const getLocationsFromReq = (request, callback) => {
	request.webtaskContext.storage.get((error, data) => {
		if (error) {
			data = {};
		}

		callback(data);
	});
};

const saveLocation = (request, location, callback) => {
	request.webtaskContext.storage.get((error, data) => {
		if (error) {
			throw new Error(storageError);
		}

		data = data || {};
		data[location.city.id] = {
			name: location.city.name,
			country: location.city.country,
			forecast: location.list
		};

		request.webtaskContext.storage.set(data, (error) => {
			if (error) {
				throw new Error(storageError);
			}

			if (typeof callback === 'function') {
				callback.call(this, data);
			}
		});
	});
};

const getContentData = (locations, selected) => {
	if (!selected || !locations.hasOwnProperty(selected)) {
		return 'No result';
	}

	let data = locations[selected].forecast;
	let sanitizedData = {};
	let i = 0;
	let iLen = data.length;
	let tmpDate;
	let tmpTime;
	let temp;

	for (; i < iLen; ++i) {
		tmpDate = data[i].dt_txt.split(/\s/);
		tmpTime = tmpDate[1].split(':');

		if (!sanitizedData.hasOwnProperty(tmpDate[0])) {
			sanitizedData[tmpDate[0]] = {
				hours: [],
				friendlyDate: moment(tmpDate[0]).format('dddd, MMMM Do YYYY')
			};
		}

		temp = {
			time: tmpTime[0] + ':' + tmpTime[1],
			temp: data[i].main.temp,
			desc: data[i].weather[0].description,
			icon: data[i].weather[0].icon,
			wind: null,
			cloud: null
		};

		if (data[i].wind.hasOwnProperty('speed')) {
			temp.wind = data[i].wind.speed;
		}

		if (data[i].clouds.hasOwnProperty('all')) {
			temp.cloud = data[i].clouds.all;
		}
		sanitizedData[tmpDate[0]].hours.push(temp);
	}

	return sanitizedData;
}

const htmlResponse = (res, req, selected) => {
	res.set('Content-Type', 'text/html');
	getLocationsFromReq(req, (locations) => {
		let sidebar = handlebars.compile(sidebarTemplate);
		let content = handlebars.compile(contentTemplate);
		let contentData = getContentData(locations, selected);
		const HTML = renderView({
			sidebar: sidebar({
				locations: locations,
				route: appPrefix + '/city/',
				selected: selected
			}),
			content: content({contentData})
			// content: JSON.stringify(contentData)
		});
		res.status(200).send(HTML);
	});
};

const renderView = (locals) => {
  return `
	<!DOCTYPE html>
	<html>
	<head>
		<meta charset="utf-8">
		<title>Webtask based Weather</title>
		<style type="text/css">
			body {
				margin: 0px 25%;
			}

			.search-form {
				padding: 20px 0;
			}

			.search-input {
				width: 220px;
				border-radius: 2px;
				box-sizing: border-box;
				padding: 4px 8px;
				border: 1px solid #DDDDDD;
			}

			.search-button {
				border: 1px solid #008FD5;
				border-radius: 2px;
				color: #FFFFFF;
				padding: 4px 8px;
				background-color: #008FD5;
				cursor: pointer;
			}

			.container {
				height: 500px;
				border: 1px solid #DDDDDD;
			}

			.container > div {
				float: left;
				height: 100%;
				overflow: auto;
			}

			.sidebar {
				width: 24%;
				border-right: 1px solid #DDDDDD;
			}

			.sidebar > a {
				display: block;
				text-align: center;
				padding: 5px 0;
				text-decoration: none;
				color: #000000;
			}

			.sidebar > a:hover,
			.sidebar > a.selected {
				background-color: #EDEDED;
			}

			.content {
				width: 75.8%;
			}

			.content table {
				width: 100%;
				border-collapse: collapse;
			}

			.weather-date > td {
				text-align: center;
				padding: 5px;
				background-color: #CCFFFF;
				border: 1px solid #ADDFFF;
			}

			.weather-info > td {
				border-top: 1px solid #ADDFFF;
				padding: 0;
			}

			.weather-info > td:first-child {
				padding-left: 10px;
				width: 5px;
			}

			.weather-info > td > p {
				margin: 0;
			}
		</style>
	</head>

	<body>
		<form class="search-form" action="${appPrefix}/search" method="POST">
			<input class="search-input" name="search" placeholder="Please enter US city or zip code"/>
			<button class="search-button">Search</button>
		</form>
		<div class="container">
			<div class='sidebar'>
				${locals.sidebar}
			</div>
			<div class='content'>
				${locals.content}
			</div>
		</div>
	</body>
	</html>
  `;
};

const sidebarTemplate = `
	{{#each locations}}
		<a href="{{../route}}{{@key}}" class="
			{{#ifCond ../selected @key}}
				selected
			{{/ifCond}}
		">{{name}}</a>
	{{/each}}
`;

const contentTemplate = `
	<table class="weather">
		<tbody>
			{{#each contentData}}
				<tr>
					<td>
						<table>
							<tbody>
								<tr class='weather-date'>
									<td colspan="3">{{friendlyDate}}</td>
								</tr>
								{{#each hours}}
									<tr class="weather-info">
										<td>
											<span class="time">{{time}}</span>
										</td>
										<td>
											<img
												src="https://openweathermap.org/img/w/{{icon}}.png"
												title="{{desc}}"/>
										</td>
										<td>
											<span class="temp">{{round temp}} °F</span>
											<span class="desc">{{desc}}</span>
											<p>
												wind: {{wind}}, m/h
												cloud: {{cloud}}%
											</p>
										</td>
									</tr>
								{{/each}}
							</tbody>
						</table>
					</td>
				</tr>
			{{/each}}
		</tbody>
	</table>
`;

const URL = 'http://samples.openweathermap.org/data/2.5/weather?zip=94040,us&appid=b1b15e88fa797225412429c1c50c122a1';

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.get('/', (req, res) => {
	htmlResponse(res, req);
});

app.get('/zip/:zip', (req, res) => {
	res.set('Content-Type', 'text/html');
	getByZip(getSecretFromReq(req), req.params.zip)
		.then(data => {
			if (data.cod === '200') {
				getByName(getSecretFromReq(req), data.city.name).then(data => {
					if (data.cod === '200') {
						saveLocation(req, data);
						const HTML = renderView({
							content: JSON.stringify(data)
						});
						res.status(200).send(HTML);
					}
				});
			}
		});
});

app.post('/search', (req, res) => {
	let queryStr = req.body.search + '';
	queryStr = queryStr.trim();

	if (!queryStr) {
		htmlResponse(res, req);
		return;
	}

	if (queryStr.length === 5 && /^\d{5}$/.test(queryStr)) {
		getByZip(getSecretFromReq(req), queryStr)
			.then(data => {
				if (data.cod === '200') {
					getByName(getSecretFromReq(req), data.city.name).then(data => {
						if (data.cod === '200') {
							saveLocation(req, data, () => {
								htmlResponse(res, req, data.city.id);
							});
						}
					});
				}
			});
	} else {
		getByName(getSecretFromReq(req), queryStr).then(data => {
			if (data.cod === '200') {
				saveLocation(req, data, () => {
					htmlResponse(res, req, data.city.id);
				});
			}
		});
	}
});

app.get('/city/:id', (req, res) => {
	getByCity(getSecretFromReq(req), req.params.id)
		.then(data => {
			saveLocation(req, data);
			htmlResponse(res, req, req.params.id);
		});
});

module.exports = fromExpress(app);
